using System;
using System.IO;
using System.Threading.Tasks;
using DocXToPdfConverter;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Hosting;

namespace BlazorApp.Data
{
    public class Converter
    {
        private static string officeLocation =
            "C:\\Users\\edson\\Downloads\\LibreOfficePortable\\App\\libreoffice\\program\\soffice.exe";

        private IWebHostEnvironment _env;

        public Converter(IWebHostEnvironment env)
        {
            _env = env;
        }

        public async Task<string> ConvertFiles(InputFileChangeEventArgs eventArgs)
        {
            Console.WriteLine("Comenzando");
            IBrowserFile file = eventArgs.GetMultipleFiles(1)[0];
            Stream stream = file.OpenReadStream();
            string path = Path.Combine(_env.WebRootPath, file.Name);
            Console.WriteLine(path);
            FileStream fs = File.Create(path);
            await stream.CopyToAsync(fs);
            stream.Close();
            fs.Close();
            Console.WriteLine("copied");
            ReportGenerator generator = new ReportGenerator(officeLocation);
            string newName = file.Name.Replace(Path.GetExtension(file.Name), ".pdf");
            Console.WriteLine(newName);
            generator.Convert(
                path,
                Path.Combine(_env.WebRootPath, newName)
            );
            string downloadPath = newName;
            File.Delete(path);
            Console.WriteLine("Finalizado");
            return newName;
        }
    }
}